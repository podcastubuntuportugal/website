+++
title = "Fala com o Podcast"
date = "2017-06-12"
aliases = ["contact", "reachme", "contacto"]
author = "Podcast Ubuntu Portugal"
+++

Enviem-nos as vossas perguntas e/ou sugestões para o nosso email:
{{< mail-link
  id="socialcontact"
  direction="contacto"
  domain="podcastubuntuportugal.org"
  fakemail="podcastubuntuportugal@dispostable.com"
  subject="Pergunta/Sugestão ao podcast"
  fakebody="Substituir o email por contacto --arroba-- podcastubuntuportugal -ponto- org"
>}}
contacto --arroba-- podcastubuntuportugal -ponto- org
{{< / mail-link >}}


Ou entrem em contacto connosco através das nossas redes sociais

- [Canal Telegram](https://t.me/PodcastUbuntuPortugal)
- [Grupo Telegram](https://t.me/ubuntuptgeral)
- [Twitter](https://t.me/PodcastUbuntuPortugal)
- [Facebook](https://www.facebook.com/podcastubuntuportugal/)
