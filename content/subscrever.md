+++
title = "Subscrever o Podcast"
date = "2017-06-12"
aliases = ["subscribe", "subscribe-to-podcast"]
author = "Podcast Ubuntu Portugal"
+++

Subscrevam o nosso Podcast, para estarem sempre a par de novos episódios.
É gratuito e podem subscrever na vossa plataforma favorita:

- [Apple Podcasts](https://podcasts.apple.com/pt/podcast/podcast-ubuntu-portugal/id1247902307)
- [Spotify](https://open.spotify.com/show/2o3xJMEAjlPdInyxjyYUe2)
- [Google Podcasts](https://podcasts.google.com/feed/aHR0cHM6Ly9wb2RjYXN0dWJ1bnR1cG9ydHVnYWwub3JnL2ZlZWQvcG9kY2FzdC8)
- [Youtube](https://www.youtube.com/podcastubuntuportugal)

Ou em qualquer plataforma através da [RSS Feed](../feed/podcast/index.xml).
