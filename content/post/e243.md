+++
title = "E243 Lagostazilla Beta"
itunes_title = "Lagostazilla Beta"
episode = 243
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e243/e243.mp3"
podcast_duration = "0:59:53"
podcast_bytes = "86247339"
author = "Podcast Ubuntu Portugal"
date = "2023-04-20"
description = "Além das nossas costumeiras aventuras semanais, que esta semana envolveram containers, Ubuntu Touch, Home Assistant e Ansible, nesta semana o maior foco tinha de ser um olhar sobre a Beta do Ubuntu 23.04 Lunar Lobster, em antecipação da release no dia 20 de Abril. Ubuntu, Ubuntu Mate, Lubuntu e Ubuntu Studio."
thumbnail = "images/e243.png"

featured = false
categories = ["Episódio"]
tags = [
  "lxc",
  "docker",
  "snap",
  "deb",
  "OTA-25",
  "hotfix",
  "Volla",
  "VollaPhone X",
  "Lunar",
  "Home Assistant",
  "Lunar Party",
  "Agenda",
  "Release Party",
  "Flutter",
  "Installer",
  "Tilling",
  "Ubuntu Studio",
  "Lubuntu",
  "Ubuntu Mate",
  "Linux",
  "Software Livre",
  "Open Source Software",
  "UBports",
  "Ubuntu Touch",
  "Ubuntu",
  "Nitrokey",
  "Humble Bundle",
  "Sr Podcast",
]
seasons = ["S01"]
aliases = ["e243", "E243"]
+++

Além das nossas costumeiras aventuras semanais, que esta semana envolveram containers, Ubuntu Touch, Home Assistant e Ansible, nesta semana o maior foco tinha de ser um olhar sobre a Beta do Ubuntu 23.04 Lunar Lobster, em antecipação da release no dia 20 de Abril. Ubuntu, Ubuntu Mate, Lubuntu e Ubuntu Studio.

Já sabem: oiçam, subscrevam e partilhem!

* https://fatela-sonica.pt
* https://nixos.org
* https://loco.ubuntu.com/events/ubuntu-pt/4323-encontro-ubuntu-pt-abril-sintra-festa-de-lançamento-do-lunar-lobster
* https://lunar.centrolinux.pt
* https://www.humblebundle.com/books/web-development-with-pragmatic-books?partner=pup
* https://www.humblebundle.com/books/pocket-guides-2023-oreilly-books?partner=pup
* https://loco.ubuntu.com/teams/ubuntu-pt/
* https://shop.nitrokey.com/shop?aff_ref=3
* https://masto.pt/@pup
* https://youtube.com/PodcastUbuntuPortugal


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

