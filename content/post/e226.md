+++
title = "E226 Drupal 10, com Ricardo Amaro"
itunes_title = "Drupal 10, com Ricardo Amaro"
episode = 226
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e226/e226.mp3"
podcast_duration = "0:56:47"
podcast_bytes = "81792312"
author = "Podcast Ubuntu Portugal"
date = "2022-12-22"
description = "Talvez embebidos no espírito natalício developers do Drupal resolveram oferecer ao mundo uma generosa dádiva!"
thumbnail = "images/e226.png"

featured = false
categories = ["Episódio"]
tags = [
  "Ubuntu",
  "Collabora",
  "Slimbook",
  "Radio Zero",
  "Sr Podcast",
]
seasons = ["S01"]
aliases = ["e226", "E226"]
+++

Talvez embebidos no espírito natalício developers do Drupal resolveram oferecer ao mundo a generosa dádiva da publicação do Drupal 10. Aqui no Podcast Ubuntu Portugal, resolvemos saber mais sobre ele e por isso convidámos o nosso amigo Ricardo Amaro da Drupal Portugal para nos explicar tudo sobre esta nova versão.
Já sabem: oiçam, subscrevam e partilhem!

Para dar uma voltinha no Drupal 10 visita: http://simplytest.me/project/drupal

* https://drupal.pt/
* https://amzn.to/3H2n9Oi
* https://amzn.to/3xtq64o
* https://amzn.to/3wOrm1k
* https://amzn.to/3Lz8vxA
* https://gitlab.com/podcastubuntuportugal
* https://shop.nitrokey.com/shop/product/nk-pro-2-nitrokey-pro-2-3?aff_ref=3
* https://shop.nitrokey.com/shop?aff_ref=3
* https://masto.pt/@pup
* https://youtube.com/PodcastUbuntuPortugal


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

