+++
title = "E42 Entre Festas"
itunes_title = "Entre Festas"
episode = 42
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e42/e042.mp3"
podcast_duration = "0:58:16"
podcast_bytes = "56357872"
author = "Podcast Ubuntu Portugal"
date = "2018-12-28"
description = "Ainda com a barriga cheia de sonhos, ainda sem o David, neste episódio discutimos projectos de redes caseiras, equipamento de gamming, novidades sobre a reforma do Direito de Autor na UE (Artigo 13), novidades do port de Ubuntu Touch para o Gemini PDA, e notícias de violação da GPL pela ASUS, Nextcloud 15, Purism Librem 5, e outras novidades da Dell, Ubuntu, e da comunidade UBports."
thumbnail = "images/e42.png"

featured = false
categories = ["Episódio"]
tags = [
  "artigo 13",
  "Libretrend",
  "Librebox",
  "Slimbook",
  "Hora Ubuntu",
  "Ubuntu Portugal",
  "Encontro",
  "Sintra",
  "Challet12",
  "Thunderclaw Studios",
  "Fosdem",
  "gamepad",
  "logitech",
  "rede",
  "networking",
  "unifi",
  "nextcloud",
  "vpn",
  "humblebundle",
  "gemini pda",
  "asus",
  "gpl",
  "eclipse",
  "python",
  "porto",
]
seasons = ["S01"]
aliases = ["e42", "E42"]
+++

Ainda com a barriga cheia de sonhos, ainda sem o David, neste episódio discutimos projectos de redes caseiras, equipamento de gamming, novidades sobre a reforma do Direito de Autor na UE (Artigo 13), novidades do port de Ubuntu Touch para o Gemini PDA, e notícias de violação da GPL pela ASUS, Nextcloud 15, Purism Librem 5, e outras novidades da Dell, Ubuntu, e da comunidade UBports.

* https://www.humblebundle.com/books/hacking-for-the-holidays-books
* https://www.xda-developers.com/asus-encrypted-kernel-source-zenfone-max-pro-m2/
* https://nextcloud.com/blog/nextcloud-15-goes-social-enforces-2fa-and-gives-you-a-new-generation-real-time-document-editing/
* https://news.softpedia.com/news/purism-ships-librem-5-dev-kits-the-linux-phones-will-arrive-in-april-2019-524319.shtml
* https://news.softpedia.com/news/ubuntu-18-04-lts-is-now-available-on-the-dell-precision-5530-and-3530-laptops-524330.shtml
* https://discourse.ubuntu.com/t/nautilus-3-30-status/8907/12
* https://slimbook.es/eclipse-linux-profesional-workstation-and-gaming-laptop


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

