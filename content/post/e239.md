+++
title = "E239 Pombo Trovejante"
itunes_title = "Pombo Trovejante"
episode = 239
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e239/e239.mp3"
podcast_duration = "0:56:49"
podcast_bytes = "81818644"
author = "Podcast Ubuntu Portugal"
date = "2023-03-23"
description = "No episódio desta semana o Miguel partilhou uma colecção de sucessos, resultado da suas recentes experiências com Docker, o Carrondo voltou às impressões 3D enquanto o Constantino navegava no mar de perguntas e respostas que é o AskUbuntu. O Bionic vai deixar-nos, o Lobster virá melhor que nunca e o Thuderbird está a pensar em meter-se no Botox."
thumbnail = "images/e239.png"

featured = false
categories = ["Episódio"]
tags = [
  "Linux",
  "Software Livre",
  "Open Source Software",
  "CentroLinux",
  "AskUbuntu",
  "Nextcloud",
  "Raspberry Pi",
  "3dPriting",
  "HomeAssistant",
  "Mycroft",
  "Podcasts",
  "Volumio",
  "18.04",
  "Bionic",
  "Lunar",
  "Mozilla",
  "Thunderbird",
  "Thundercast",
  "Ubuntu",
  "Humble Bundle",
  "Sr Podcast",
]
seasons = ["S01"]
aliases = ["e239", "E239"]
+++

No episódio desta semana o Miguel partilhou uma colecção de sucessos, resultado da suas recentes experiências com Docker, o Carrondo voltou às impressões 3D enquanto o Constantino navegava no mar de perguntas e respostas que é o AskUbuntu. O Bionic vai deixar-nos, o Lobster virá melhor que nunca e o Thuderbird está a pensar em meter-se no Botox.

Já sabem: oiçam, subscrevam e partilhem!

* https://askubuntu.com/questions/1276281/does-ubuntu-installer-preserve-btrfs-home-subvolume-while-installing-to-the/
* https://askubuntu.com/users/856493/jo%c3%a3o-manuel-rodrigues
* https://ubuntu.com/blog/18-04-end-of-standard-support
* https://www.zdnet.com/article/ubuntu-lunar-lobster-could-be-the-surprise-hit-of-2023/
* https://discourse.ubuntu.com/t/lunar-lobster-release-schedule/27284
* https://discourse.ubuntu.com/t/lunar-lobster-release-notes/31910
* https://www.phoronix.com/news/Ubuntu-23.04-Linux-6.2-Coming
* https://www.omgubuntu.co.uk/2023/03/ubuntu-23-04-default-wallpaper
* https://www.omgubuntu.co.uk/2023/03/ubuntu-23-04-will-ship-with-linux-kernel-6-2
* https://www.omgubuntu.co.uk/2023/03/ubuntu-23-04-features
* https://www.humblebundle.com/books/cybersecurity-packt-2023-books?partner=PUP
* https://www.humblebundle.com/books/linux-mega-bundle-packt-books?partner=PUP
* https://www.humblebundle.com/books/cookbooks-for-coders-books?partner=PUP
* https://blog.thunderbird.net/
* https://blog.thunderbird.net/2023/02/thunderbird-115-supernova-preview-the-new-folder-pane/
* https://blog.thunderbird.net/2023/02/the-future-of-thunderbird-why-were-rebuilding-from-the-ground-up/
* https://blog.thunderbird.net/2023/03/thundercast-1-origin-stories/
* https://www.youtube.com/watch?v=EoLb6aHakno
* https://www.youtube.com/watch?v=P28jZTobvM4
* https://pt.wikimedia.org/wiki/WikiCon_Portugal_2023
* https://web.archive.org/web/20230322085110/https://pt.wikimedia.org/wiki/WikiCon_Portugal_2023
* https://loco.ubuntu.com/teams/ubuntu-pt/
* https://shop.nitrokey.com/shop?aff_ref=3
* https://masto.pt/@pup
* https://youtube.com/PodcastUbuntuPortugal


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

