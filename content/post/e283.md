+++
title = "E283 Dia da Protecção de Dados e Privacidade"
itunes_title = "Dia da Protecção de Dados e Privacidade"
episode = 283
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e283/e283.mp3"
podcast_duration = "0:56:35"
podcast_bytes = "27167972"
author = "Podcast Ubuntu Portugal"
date = "2024-01-25"
description = "Neste dia muito caro para nós, recebemos um convidado especial: Francisco Core, da PrivacyLX. Numa conversa animada sobre Privacidade e Direitos Digitais, foram abordados os medos, inquietações, ansiedades e esperanças sobre o estado actual da privacidade no mundo digital, a sua realidade portuguesa e internacional e as tendências para onde nos levam. Uma questão complexa, multidimensional e que obviamente não cabe num segmento de uma hora - mas esperamos que pelo menos desperte o interesse de todos para algo que tem uma importância extrema (ainda que pouco visível) nas nossas vidas."
thumbnail = "images/e283.png"

featured = false
categories = ["Episódio"]
tags = [
  "Home Assistant",
  "Privacy Café",
  "PrivacyLx",
  "Privacidade",
  "Protecção de Dados",
  "Direitos Digitais",
  "Ubuntu",
  "GNU/Linux",
  "Linux",
  "Software Livre",
  "Open Source Software",
  "Sr Podcast",
]
seasons = ["S01"]
aliases = ["e283", "E283"]
+++

Neste dia muito caro para nós, recebemos um convidado especial: Francisco Core, da PrivacyLX. Numa conversa animada sobre Privacidade e Direitos Digitais, foram abordados os medos, inquietações, ansiedades e esperanças sobre o estado actual da privacidade no mundo digital, a sua realidade portuguesa e internacional e as tendências para onde nos levam. Uma questão complexa, multidimensional e que obviamente não cabe num segmento de uma hora - mas esperamos que pelo menos desperte o interesse de todos para algo que tem uma importância extrema (ainda que pouco visível) nas nossas vidas.

Já sabem: oiçam, subscrevam e partilhem!

- https://privacyguides.org
- https://privacylx.org
- https://mastodon.social/@privacylx
- https://www.direitosdigitais.pt
- https://www.ansol.org
- https://themarkup.org/privacy/2024/01/17/each-facebook-user-is-monitored-by-thousands-of-companies-study-indicates
- https://innovation.consumerreports.org/wp-content/uploads/2024/01/CR_Who-Shares-Your-Information-With-Facebook.pdf
- https://darkvisitors.com/agents
- https://www.episodiqu.es/p/presse-et-ia-le-syndrome-du-scorpion
- https://web.archive.org/web/20240125120344/https%3A%2F%2Fwww.rtp.pt%2Fnoticias%2Fpais%2Fjorge-silva-carvalho-condenado-a-4-anos-e-meio-de-prisao-com-pena-suspensa_v963085
- https://web.archive.org/web/20240125115823/https%3A%2F%2Fwww.tsf.pt%2Fsociedade%2Fjustica%2Frelacao-de-lisboa-rejeita-recurso-de-ex-diretor-do-sied-no-caso-das-secretas-9174713.html%2F
- https://pt.wikipedia.org/wiki/Jorge_Manuel_Jacob_da_Silva_de_Carvalho

- https://loco.ubuntu.com/teams/ubuntu-pt/
- https://shop.nitrokey.com/shop?aff_ref=3
- https://masto.pt/@pup
- https://youtube.com/PodcastUbuntuPortugal


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

