+++
title = "E119 Queijo de Cabra"
itunes_title = "Queijo de Cabra"
episode = 119
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e119/e119.mp3"
podcast_duration = "0:57:00"
podcast_bytes = "55176830"
author = "Podcast Ubuntu Portugal"
date = "2020-12-03"
description = "Tornamos histórias enfadonhas em aventuras fantásticas, acontecimentos cinzentos em verdadeiros contos de fadas, ou então falamos só sobre Ubuntu e outras cenas… Aqui fica mais um episódio no vosso podcast preferido."
thumbnail = "images/e119.png"

featured = false
categories = ["Episódio"]
tags = [
  "podcast",
  "phoenix phones",
  "moodle",
  "supertuxkart",
  "encontro comunidade",
  "snapcraft",
  "notes",
  "joplin",
  "docker",
  "Nitrokey",
  "LibreTrend",
  "Slimbook",
  "Radio Zero",
  "Sr Podcast",
]
seasons = ["S01"]
aliases = ["e119", "E119"]
+++

Tornamos histórias enfadonhas em aventuras fantásticas, acontecimentos cinzentos em verdadeiros contos de fadas, ou então falamos só sobre Ubuntu e outras cenas… Aqui fica mais um episódio no vosso podcast preferido.
Já sabem: oiçam, subscrevam e partilhem!

* https://redcircle.com/shows/phoenix-phones
* https://www.digitalocean.com/community/tech_talks/utilizing-security-features-in-ssh
* https://shop.nitrokey.com/de_DE/shop/product/nk-pro-2-nitrokey-pro-2-3?aff_ref=3
* https://ubuntu.com/blog/canonical-publishes-lts-docker-image-portfolio-on-docker-hub
* https://ubuntu.com/blog/how-to-make-snaps-and-configuration-management-tools-work-together
* https://forum.snapcraft.io/t/call-for-suggestions-featured-snaps-friday-9th-october-2020/20384
* https://snapstats.org/snaps/flameshot
* https://snapcraft.io/joplin-james-carroll
* https://joplinapp.org/
* https://snapcraft.io/glow
* https://forum.snapcraft.io/t/snapcraft-clinic/20166
* https://shop.nitrokey.com/de_DE/shop?aff_ref=3



### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

