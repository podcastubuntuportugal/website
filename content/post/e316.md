+++
title = "E316 Meu Velho, Meu Amigo."
itunes_title = "Meu Velho, Meu Amigo."
episode = 316
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e316/e316.mp3"
podcast_duration = "0:56:08"
podcast_bytes = "26949798"
author = "Podcast Ubuntu Portugal"
date = "2024-09-19"
description = "O Ubuntu faz 20 anos! Esta semana dedicámo-nos a um passatempo: reabilitar computadores velhinhos, com 18 e 25 anos respectivamente. Para esse efeito, experimentámos Debian 12 e ficámos surpreendidos com o resultado. Ainda houve tempo para bonitas músicas Ubuntu, antecipar a próxima versão de Ubuntu Touch baseada em Noble, dar parabéns à Canonical e rever a agenda de eventos das próximas semanas, no Porto, Aveiro, Haia e Belém. Já não falta muito para a Festa do Software Livre...!"
thumbnail = "images/e316.png"

featured = false
categories = ["Episódio"]
tags = [
  "Noble Numbat",
  "Debian 12",
  "32bit",
  "i386",
  "Spinrite",
  "DevOPS",
  "Ubucon",
  "Festa do Software Livre",
  "KCD",
  "Canonical",
  "UBports",
  "Ubuntu",
  "GNU/Linux",
  "Linux",
  "Software Livre",
  "Open Source Software",
  "Sr Podcast",
]
seasons = ["S01"]
aliases = ["e316", "E316"]
+++

O Ubuntu faz 20 anos! Esta semana dedicámo-nos a um passatempo: reabilitar computadores velhinhos, com 18 e 25 anos respectivamente. Para esse efeito, experimentámos Debian 12 e ficámos surpreendidos com o resultado. Ainda houve tempo para bonitas músicas Ubuntu, antecipar a próxima versão de Ubuntu Touch baseada em Noble, dar parabéns à Canonical e rever a agenda de eventos das próximas semanas, no Porto, Aveiro, Haia e Belém. Já não falta muito para a Festa do Software Livre...!

Já sabem: oiçam, subscrevam e partilhem!
  
- Spinrite: https://www.grc.com/sr/spinrite.htm
- Fio de Restauro: https://ciberlandia.pt/@per_sonne/113131712075237725
- Mimi Browser: https://open-store.io/app/mimibrowser.fredldotme
- Devopsdays Portugal: https://devopsdays.org/events/2024-portugal/welcome/
- KCD Porto: https://community.cncf.io/events/details/cncf-kcd-porto-presents-kcd-porto-2024/
- Festa do Software Livre: https://festa2024.softwarelivre.eu/
- Ubucon Portugal: https://ubuconpt2024.ubuntu-pt.org
- Ubuntu Summit: 
 - https://events.canonical.com/event/51/
 - https://ubuntu.com/blog/ubuntu-summit-2024-a-logo-takes-flight
- https://www.omgubuntu.co.uk/2024/09/ubuntus-new-security-center-readies-stable-release
- https://discourse.ubuntu.com/t/ubuntu-desktop-s-24-10-dev-cycle-part-5-introducing-permissions-prompting/47963
- FOSS4G World: https://2024.foss4g.org/
- Meu Velho, Meu Amigo: https://youtu.be/4692_ah5aRU
- https://loco.ubuntu.com/teams/ubuntu-pt/
- https://shop.nitrokey.com/shop?aff_ref=3
- https://masto.pt/@pup
- https://youtube.com/PodcastUbuntuPortugal


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE). (https://creativecommons.org/licenses/by/4.0/). A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

