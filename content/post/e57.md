+++
title = "E57 O bom, o mau e o lambão"
itunes_title = "O bom, o mau e o lambão"
episode = 57
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e57/e057.mp3"
podcast_duration = "1:08:23"
podcast_bytes = "66136760"
author = "Podcast Ubuntu Portugal"
date = "2019-06-10"
description = "Neste episódio ficámos a saber quais foram os últimos destinos das viagens do Diogo Constantino, onde é que ele andou a gastar dinheiro, mas também o que andou o Tiago a fazer para não ter estado nos últimos episódios. Já sabes, ouve, subscreve e partilha!"
thumbnail = "images/e57.png"

featured = false
categories = ["Episódio"]
tags = [
  "NOT YET",
]
seasons = ["S01"]
aliases = ["e57", "E57"]
+++

Neste episódio ficámos a saber quais foram os últimos destinos das viagens do Diogo Constantino, onde é que ele andou a gastar dinheiro, mas também o que andou o Tiago a fazer para não ter estado nos últimos episódios. Já sabes, ouve, subscreve e partilha!

* https://sintra2019.ubucon.org/
* https://videos.ubuntu-paris.org/
* https://slimbook.es/zero-smart-thin-client-linux-windows-fanless
* https://slimbook.es/pedidos/mandos/mando-gaming-inal%C3%A1mbrico-nox-comprar
* https://panopticlick.eff.org/
* https://www.mozilla.org/en-US/firefox/67.0/releasenotes/
* https://blog.mozilla.org/addons/2019/03/26/extensions-in-firefox-67/
* https://discourse.ubuntu.com/t/mir-1-2-0-release/11034
* https://www.linuxondex.com/
* https://github.com/tcarrondo/aws-mfa-terraform
* https://devblogs.microsoft.com/commandline/announcing-wsl-2/


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

