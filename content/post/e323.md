+++
title = "E323 Balanço de Cimeiras"
itunes_title = "Balanço de Cimeiras"
episode = 323
podcast_file = "https://hub.podcastubuntuportugal.org/s/a8HgZ54LHJyDWqT/download/e323.mp3"
podcast_duration = "0:58:04"
podcast_bytes = "27875995"
author = "Podcast Ubuntu Portugal"
date = "2024-11-07"
description = "O nosso enviado especial regressou dos Países Baixos, onde assistiu a todos os pontos altos da Cimeira do Ubuntu e partilhou com o estimado público os detalhes desse grandioso certame, que maravilhou todos quantos a ele acorreram, numa eloquente mostra dos avanços da ciência, tecnologia e saber que impulsionam o progresso da Humanidade na direcção de um porvir radiante! [Lamentamos a linguagem, o nosso estagiário acordou nos anos 1930, neste dia 7 de Novembro]"
thumbnail = "images/e323.png"

featured = false
categories = ["Episódio"]
tags = [
  "Ubuntu Summit",
  "UbuconPT2024",
  "FestaSoftwareLivre2024",
  "Ubuntu",
  "GNU/Linux",
  "Linux",
  "Software Livre",
  "Open Source Software",
  "Sr Podcast",
]
seasons = ["S01"]
aliases = ["e323", "E323"]
+++

O nosso enviado especial regressou dos Países Baixos, onde assistiu a todos os pontos altos da Cimeira do Ubuntu e partilhou com o estimado público os detalhes desse grandioso certame, que maravilhou todos quantos a ele acorreram, numa eloquente mostra dos avanços da ciência, tecnologia e saber que impulsionam o progresso da Humanidade na direcção de um porvir radiante! [Lamentamos a linguagem, o nosso estagiário acordou nos anos 1930, neste dia 7 de Novembro]

Já sabem: oiçam, subscrevam e partilhem!
  
- Ubuntu Summit 2024 (Haia, 25-27 Out.): https://events.canonical.com/event/51/
- Vídeos da Cimeira: https://youtube.com/playlist?list=PLwFSk464RMxnAWg_c9RkUt33FqzwOZfg7
- Tonor: https://www.tonormic.com/collections/wireless-microphones/products/wireless-lavalier-microphones?variant=45251902538009
- Ovomaltine: https://youtu.be/r1CcC284qJo
- Ovaltine: https://youtu.be/MQXcFficxVU
- https://loco.ubuntu.com/teams/ubuntu-pt/
- https://shop.nitrokey.com/shop?aff_ref=3
- https://masto.pt/@pup
- https://youtube.com/PodcastUbuntuPortugal


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE). (https://creativecommons.org/licenses/by/4.0/). A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

