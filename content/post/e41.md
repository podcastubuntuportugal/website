+++
title = "E41 Arquivo McCloud MacCloudface"
itunes_title = "Arquivo McCloud MacCloudface"
episode = 41
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e41/e041.mp3"
podcast_duration = "1:01:30"
podcast_bytes = "59168321"
author = "Podcast Ubuntu Portugal"
date = "2018-12-20"
description = "Precisam de uma dose de conversa tecnológica para acompanhar filhoses e rabanadas?"
thumbnail = "images/e41.png"

featured = false
categories = ["Episódio"]
tags = [
  "Ubuntu",
  "Ubuntu Hour",
  "Ubuntu-pt",
  "Save Your Internet",
  "UBports",
  "UbuntuTouch OTA6",
  " Mozilla Firefox",
  "Libretrend Librebox",
  "LXD",
  "Sintra",
  "Ubuntu Party",
  "Paris",
  "Free Music Archive",
  "Microsoft Edge",
  "Descontos",
  "Slimbook",
  "Artigo 13",
  "Save Your Internet",
  "LinuxTech",
  "Festa de Software Livre Moita 2018",
  "Streaming",
  "Reddit",
  "Telegram",
  "hackathon",
  "Xiami 4",
  "Planet Computers",
  "Gemini PDA",
  "OTA7",
  "Ubucon Europe",
  "FOSDEM",
]
seasons = ["S01"]
aliases = ["e41", "E41"]
+++

Precisam de uma dose de conversa tecnológica para acompanhar filhoses e rabanadas?
Então estão safos, porque já saiu o Arquivo McCloud MacCloudface.
Neste episódio falámos da Ubuntu Hour sobre LXD, da Ubuntu Party de Paris, Artigo 13, notícias do UBports OTA6 e hardware, releases do Firefox 64 e LXD 3.8 e discutimos acontecimentos recentes do ecossistema da web.

* http://freemusicarchive.org/member/cheyenne_h/blog/Free_Music_Archives_new_home_KitSplit
* https://ubuntu-paris.org/
* https://www.youtube.com/watch?v=wSeYiVsMTew
* https://www.youtube.com/watch?v=XEfjXeg6TVk
* https://www.youtube.com/watch?v=IzuE-Bitmwc
* https://linuxtech.pt/
* https://www.reddit.com/r/portugal/comments/a58ftc/podcast_em_portugu%C3%AAs_sobre_ubuntu_software_livre/
* https://ubports.com/blog/ubports-blog-1/tag/blogs-2
* https://www.youtube.com/watch?v=I-zNSSMWdIY
* https://peer.tube/videos/watch/097050d3-ffa8-4272-938c-67f24826b2ac
* https://gettogether.community/events/518/ut-telegram-client-hackathon-part-ii/
* https://forums.ubports.com/topic/2069/for-device-tarball-question
* https://twitter.com/shenfeng020/status/1072761057066799104
* https://t.me/ubports/176275
* https://t.me/ubports/176277
* https://t.me/ubports/176278
* https://t.me/ubports/176279
* https://www.mozilla.org/en-US/firefox/64.0/releasenotes/
* https://www.geekboots.com/news/firefox-64-coming-with-lot-of-improvements
* https://www.zdnet.com/article/firefox-64-released-with-a-windows-like-task-manager/
* https://discuss.linuxcontainers.org/t/lxd-3-8-has-been-released/3450
* https://pythonporto.org/
* https://fosdem.org/2019/
* http://loco.ubuntu.com/events/ubuntu-pt/3827-hora-ubuntu-dom%C3%B3tica-com-o-homeassistant-sintra/
* http://loco.ubuntu.com/events/ubuntu-pt/3824-encontro-ubuntu-pt-sintra/


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

