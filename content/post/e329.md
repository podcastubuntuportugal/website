+++
title = "E329 Serial com Fibra"
itunes_title = "Serial com Fibra"
episode = 329
podcast_file = "https://hub.podcastubuntuportugal.org/s/AND6rDcETSXr5F3/download/e329.mp3"
podcast_duration = "0:58:58"
podcast_bytes = "28310046"
author = "Podcast Ubuntu Portugal"
date = "2024-12-19"
description = "O Diogo viajou até à Idade Média e descobriu a sensação de viver num tempo diferente, em que não tem velocidades de 1Gbps! Uma barbaridade! O Miguel viajou até à Idade do Cobre e relata-nos o que é viver com apenas 100 Mpbs. Depois, viajaram os dois até à Idade do Alumínio Anodizado, abrindo um pacote com uma misteriosa caixa, de um azul faíscante...que surpresas encerrará?!"
thumbnail = "images/e329.png"

featured = false
categories = ["Episódio"]
tags = [
  "Digi",
  "NOS",
  "MEO",
  "ANACOM",
  "Fibra",
  "OpenWRT",
  "BananaPi",
  "NUC",
  "Intel N100",
  "Slimbook Zero",
  "Home Assistant",
  "ExPhone",
  "Ubuntu",
  "GNU/Linux",
  "Linux",
  "Software Livre",
  "Open Source Software",
  "Sr Podcast",
]
seasons = ["S01"]
aliases = ["e329", "E329"]
+++

O Diogo viajou até à Idade Média e descobriu a sensação de viver num tempo diferente, em que não tem velocidades de 1Gbps! Uma barbaridade! O Miguel viajou até à Idade do Cobre e relata-nos o que é viver com apenas 100 Mpbs. Depois, viajaram os dois até à Idade do Alumínio Anodizado, abrindo um pacote com uma misteriosa caixa, de um azul faíscante...que surpresas encerrará?!

Já sabem: oiçam, subscrevam e partilhem!
  
- OpenWRT: https://liliputing.com/openwrt-one-wifi-6-router-is-now-available-for-89/
- Preços OpenWRT: https://sfconservancy.org/news/2024/nov/29/openwrt-one-wireless-router-now-ships-black-friday/
- Slimbook Zero: https://slimbook.com/zero
- Intel N100: https://www.intel.com.br/content/www/br/pt/products/sku/231803/intel-processor-n100-6m-cache-up-to-3-40-ghz/specifications.html
- Intel N100 (Wikipedia): https://en.wikipedia.org/wiki/Alder_Lake
- ExPhone: https://open-store.io/app/com.github.jmlich.exphone
- LoCo PT: https://loco.ubuntu.com/teams/ubuntu-pt/
- Nitrokey: https://shop.nitrokey.com/shop?aff_ref=3
- Mastodon: https://masto.pt/@pup
- Youtube: https://youtube.com/PodcastUbuntuPortugal


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE). (https://creativecommons.org/licenses/by/4.0/). A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

