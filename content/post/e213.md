+++
title = "E213 Lucas Lasota"
itunes_title = "Lucas Lasota"
episode = 213
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e213/e213.mp3"
podcast_duration = "1:21:56"
podcast_bytes = "117989876"
author = "Podcast Ubuntu Portugal"
date = "2022-09-15"
description = "Em período de férias, estivemos à conversa com Lucas Lasota."
thumbnail = "images/e213.png"

featured = false
categories = ["Episódio"]
tags = [
  "FSFE",
  "Ubuntu",
  "Collabora",
  "Slimbook",
  "Radio Zero",
  "Sr Podcast",
]
seasons = ["S01"]
aliases = ["e213", "E213"]
+++

Em período de férias, estivemos à conversa com o Lucas Lasota, brasileiro a morar na Alemanha, advogado de profissão, trabalha no sector jurídico na FSFE (Free Software Foundation Europe). Falou-se sobre questões legais no geral mas também sobre a tematica router freedom. Novamente uma conversa interessantissima sobre Ubuntu e outras cenas...
Já sabem: oiçam, subscrevam e partilhem!

* https://wiki.fsfe.org/Activities/CompulsoryRouters/
* https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=uriserv:OJ.L_.2015.310.01.0001.01.ENG
* https://fsfe.org/news/2022/news-20220628-01.en.html
* https://nextcloud.com/blog/youre-invited-nextcloud-conference-on-october-1-2-in-berlin/
* https://www.humblebundle.com/books/linux-no-starch-press-books?partner=PUP
* https://amzn.to/3xtq64o
* https://amzn.to/3wOrm1k
* https://amzn.to/3Lz8vxA
* https://gitlab.com/podcastubuntuportugal
* https://shop.nitrokey.com/shop/product/nk-pro-2-nitrokey-pro-2-3?aff_ref=3
* https://shop.nitrokey.com/shop?aff_ref=3
* https://youtube.com/PodcastUbuntuPortugal


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

