+++
title = "E85 Pinheirinho de Páscoa"
itunes_title = "Pinheirinho de Páscoa"
episode = 85
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e85/e085.mp3"
podcast_duration = "0:55:12"
podcast_bytes = "53131748"
author = "Podcast Ubuntu Portugal"
date = "2020-04-10"
description = "O Tiago continua a tomar controlo dos meios de impressão, para ajudar os verdadeiramente heróicos trabalhadores que sustentam a nossa a sociedade nestes difíceis tempos. O Diogo anda a montar Wifi Access Points com Ubuntu."
thumbnail = "images/e85.png"

featured = false
categories = ["Episódio"]
tags = [
  "WSL",
  "WSLConf",
  "kubernetes",
  "k8s",
  "Docker",
  "Wifi",
  "snap",
  "wifi-ap",
  "LXD",
  "UBports",
  "UBports Installer",
  "Redmi Note 7",
  "Pine64",
  "PinePhone",
  "PinePhone UBports Community Edition",
  "Ubuntu",
]
seasons = ["S01"]
aliases = ["e85", "E85"]
+++

O Tiago continua a tomar controlo dos meios de impressão, para ajudar os verdadeiramente heróicos trabalhadores que sustentam a nossa a sociedade nestes difíceis tempos. O Diogo anda a montar Wifi Access Points com Ubuntu.
Nas notícias temos a nova release do LXD, o port de Ubuntu Touch para o Redmi Note 7, o anúncio da pré-venda do Pine64 PinePhone UBports Community Edition.

* https://snapcraft.io/wifiap-consumer
* https://discourse.ubuntu.com/t/ubuntu-20-04-testing-week/15043
* https://discuss.linuxcontainers.org/t/lxd-4-0-lts-has-been-released/7231
* https://ubports.com/pt_PT/blog/ubports-blog-1/post/pinephone-ubports-community-edition-pre-orders-are-open-271
* https://store.pine64.org/?product=pinephone-community-edition-ubports-limited-edition-linux-smartphone
* https://www.pine64.org/2020/04/02/pinephone-ubports-community-edition-pre-orders-now-open/


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

