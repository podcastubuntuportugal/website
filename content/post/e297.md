+++
title = "E297 Framboesa 3.1416"
itunes_title = "Framboesa 3.1416"
episode = 297
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e297/e297.mp3"
podcast_duration = "0:59:05"
podcast_bytes = "28368142"
author = "Podcast Ubuntu Portugal"
date = "2024-05-02"
description = "Depois da catástrofe de Alderaan, a princesa Leia no exílio veio falar connosco e passámos um bom bocado a falar de Raspberry Pi, as tendências genocidas do Império Galáctico, o papel de jornalistas e developers místicos e como fazer parte dos Rebeldes usando Tor. Abordámos brevemente o drama dos debs no Noble Numbat, dramas na comunidade de Nix e o drama de não fazer backups. E ainda houve tempo para partilhar experiências no Centro Linux, dizer mal do  Linkedin e rever a agenda para os próximos meses."
thumbnail = "images/e297.png"

featured = false
categories = ["Episódio"]
tags = [
  "24.04",
  "NobleNumbat",
  "CentroLinux",
  "Meetup",
  "Kodi",
  "snap",
  "deb",
  "Tor",
  "Google",
  "nix",
  "Linkedin",
  "SIG",
  "Foss4g",
  "Ubuntu",
  "GNU/Linux",
  "Linux",
  "Software Livre",
  "Open Source Software",
  "Sr Podcast",
]
seasons = ["S01"]
aliases = ["e297", "E297"]
+++

Depois da catástrofe de Alderaan, a princesa Leia no exílio veio falar connosco e passámos um bom bocado a falar de Raspberry Pi, as tendências genocidas do Império Galáctico, o papel de jornalistas e developers místicos e como fazer parte dos Rebeldes usando Tor. Abordámos brevemente o drama dos debs no Noble Numbat, dramas na comunidade de Nix e o drama de não fazer backups. E ainda houve tempo para partilhar experiências no Centro Linux, dizer mal do  Linkedin e rever a agenda para os próximos meses.

Já sabem: oiçam, subscrevam e partilhem!

- https://www.bertrand.pt/livro/os-tres-d-dos-media-jose-nuno-matos/25461378  
- https://www.torproject.org/
- https://raspberrytips.com/install-tor-browser-on-raspberry-pi/
- https://ciberlandia.pt/@per_sonne/112342621071193602
- https://flathub.org/apps/com.github.huluti.Curtail
- https://mastodon.social/@eugenialoli/112358645417203659
- https://aux.computer/
- https://thegeomob.com/post/may-8th-2024-geomoblx-details
- https://drupaliberia.eu/
- https://lisbon.globalappsec.org/
- https://developer.ogc.org/sprints/
- https://2024.europe.foss4g.org/
- https://www.wearedevelopers.com/world-congress
- https://2024.foss4g.org/

- https://loco.ubuntu.com/teams/ubuntu-pt/
- https://shop.nitrokey.com/shop?aff_ref=3
- https://masto.pt/@pup
- https://youtube.com/PodcastUbuntuPortugal


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

