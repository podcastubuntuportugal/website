+++
title = "E43 Este ano é que vai ser!"
itunes_title = "Este ano é que vai ser!"
episode = 43
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e43/e043.mp3"
podcast_duration = "1:53:53"
podcast_bytes = "110635762"
author = "Podcast Ubuntu Portugal"
date = "2019-01-03"
description = "No primeiro episódio do ano e no primeiro que contámos com toda a equipa de produção do show, fizemos uma análise retrospectiva do último ano."
thumbnail = "images/e43.png"

featured = false
categories = ["Episódio"]
tags = [
  "Ubuntu",
  "2018",
  "2019",
  "Thunderclaw Studios",
]
seasons = ["S01"]
aliases = ["e43", "E43"]
+++

No primeiro episódio do ano e no primeiro que contámos com toda a equipa de produção do show, fizemos uma análise retrospectiva do último ano.
Muitas coisas mudaram e aconteceram no podcast, no Ubuntu, no Software Livre e no mundo da tecnologia, e nós conversámos todos um pouco sobre isso.
Mas nós não nos ficámos por aí, também fizemos previsões sobre o que vai acontecer neste ano de 2019, em forma de jogo em que apostamos uns com os outros.
Se quiserem enviar-nos as vossas previsões podem enviar para contacto@podcastubuntuportugal.org.
Os patronos podem também votar nas nossas previsões no post que deixámos no patreon.
Já sabes: Ouve, subscreve e partilha!


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

