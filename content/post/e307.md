+++
title = "E307 Dégradés de Ultravioletas"
itunes_title = "Dégradés de Ultravioletas"
episode = 307
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e307/e307.mp3"
podcast_duration = "0:57:30"
podcast_bytes = "27603068"
author = "Podcast Ubuntu Portugal"
date = "2024-07-11"
description = "Um Raspberry Pi serve para muita coisa: testar Plasma e XFCE, trabalhar o dia todo a ouvir podcasts; saber quando devemos sair à rua para apanharmos um melanoma - e muito mais! O Miguel continua a saborear ambientes gráficos num Pi 5 e a brincar com automatizações e dégradés de cores com Home Assistant; mas vale a pena gastar dinheiro com mais aparelhos? Meh. O Diogo teve uma recaída no número de abas abertas; actualizou o seu Ubuntu mesmo em cima do prazo; destruiu o seu Thunderbird; promoveu descaradamente o Outro Podcast...e está a habilitar-se a arranjar problemas com o Роскомнадзор da Federação Russa e com a República Popular da China. Será que o voltaremos a ver depois de ir de férias?..."
thumbnail = "images/e307.png"

featured = false
categories = ["Episódio"]
tags = [
  "24.04",
  "Thunderbird",
  "VDD",
  "VozDosDireitosDigitais",
  "RaspberryPi",
  "Home Assistant",
  "Xfce",
  "Plasma",
  "LXDE",
  "LXQT",
  "Debian",
  "Mozilla",
  "Apple",
  "Ubuntu",
  "GNU/Linux",
  "Linux",
  "Software Livre",
  "Open Source Software",
  "Sr Podcast",
]
seasons = ["S01"]
aliases = ["e307", "E307"]
+++

Um Raspberry Pi serve para muita coisa: testar Plasma e XFCE, trabalhar o dia todo a ouvir podcasts; saber quando devemos sair à rua para apanharmos um melanoma - e muito mais! O Miguel continua a saborear ambientes gráficos num Pi 5 e a brincar com automatizações e dégradés de cores com Home Assistant; mas vale a pena gastar dinheiro com mais aparelhos? Meh. O Diogo teve uma recaída no número de abas abertas; actualizou o seu Ubuntu mesmo em cima do prazo; destruiu o seu Thunderbird; promoveu descaradamente o Outro Podcast...e está a habilitar-se a arranjar problemas com o Роскомнадзор da Federação Russa e com a República Popular da China. Será que o voltaremos a ver depois de ir de férias?...

Já sabem: oiçam, subscrevam e partilhem!
  
- https://invidious.private.coffee/watch?v=XMA8e4r1AUQ
- https://invidious.incogniweb.net/watch?v=odG7FbptgWQ
- https://invidious.private.coffee/watch?v=Dn5eowo7_Qw
- https://invidious.private.coffee/watch?v=B9OwWnAftb4
- https://shop.pimoroni.com/products/nvme-base?variant=41219587178579
- https://geekworm.com/products/x1002
- https://en.wikipedia.org/wiki/Ultraviolet_index
- https://community.home-assistant.io/t/change-colors-for-gauge-card/111624
- https://community.home-assistant.io/t/gauge-card-how-to-get-a-gradually-changing-color/486987/30
- https://ha.labtool.pl/
- https://direitosdigitais.libsyn.com/
- https://web.archive.org/web/20240709122412/https://www.bleepingcomputer.com/news/technology/russia-forces-apple-to-remove-dozens-of-vpn-apps-from-app-store/
- https://en.wikipedia.org/wiki/Roskomnadzor
- https://en.wikipedia.org/wiki/Interkosmos
- https://pt.wikipedia.org/wiki/Ernest_Henry_Shackleton
- https://loco.ubuntu.com/teams/ubuntu-pt/
- https://shop.nitrokey.com/shop?aff_ref=3
- https://masto.pt/@pup
- https://youtube.com/PodcastUbuntuPortugal


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

