+++
title = "E115 Amanhecer"
itunes_title = "Amanhecer"
episode = 115
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e115/e115.mp3"
podcast_duration = "0:57:00"
podcast_bytes = "55191467"
author = "Podcast Ubuntu Portugal"
date = "2020-11-05"
description = "Chegámos a Novembro, muito animados que o final de 2020 está cada vez mais próximo e temos esperança que o upgrade para 2021 venha corrigir parte dos bugs desta versão que claramente saiu torta… Aqui fica mais um episódio no vosso podcast preferido."
thumbnail = "images/e115.png"

featured = false
categories = ["Episódio"]
tags = [
  "opensourcelisbon",
  "open",
  "source",
  "lisbon",
  "nitrokey",
  "volla",
  "comunidade",
  "ubports",
  "pinetab",
  "podes",
  "Groovy",
  "Gorilla",
  "Ubuntu",
  "LibreTrend",
  "Slimbook",
  "Radio Zero",
  "Sr Podcast",
]
seasons = ["S01"]
aliases = ["e115", "E115"]
+++

Chegámos a Novembro, muito animados que o final de 2020 está cada vez mais próximo e temos esperança que o upgrade para 2021 venha corrigir parte dos bugs desta versão que claramente saiu torta… Aqui fica mais um episódio no vosso podcast preferido.
Já sabem: oiçam, subscrevam e partilhem!

* https://www.indiegogo.com/projects/pro1-x-smartphone-functionality-choice-control#/
* https://shop.nitrokey.com/de_DE/shop?aff_ref=3
* https://twitter.com/hello_volla/status/1319976344390950917
* https://www.meshtastic.org/
* https://forum.pine64.org/showthread.php?tid=11772
* https://twitter.com/thepine64/status/1314911896177389570
* https://www.pine64.org/2020/10/15/update-new-hacktober-gear/
* https://pine64.com/product/pinephone-community-edition-3gb-32gb-mainboard-special-offer-for-braveheart-and-ubports-owners/?v=0446c16e2e66
* https://twitter.com/braam_martijn/status/1322949166042107909
* https://podes.pt/programa/
* https://www.opensourcelisbon.com
* https://www.facebook.com/events/3574353429252373/
* https://www.humblebundle.com/software/stem-productivity-library-mercury-books?partner=PUP



### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

