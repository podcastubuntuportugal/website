+++
title = "E188 Ápêpê 1000"
itunes_title = "Ápêpê 1000"
episode = 188
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e188/e188.mp3"
podcast_duration = "0:59:16"
podcast_bytes = "57197872"
author = "Podcast Ubuntu Portugal"
date = "2022-03-31"
description = "São tempos de mudança! …pelo menos para o outro podcast do Carrondo. Já o Firefox do Miguel continua doente e o Constantino foi ao Algarve, mas a fingir. Há novidades, para variar, no mundo UBPorts e o Mark vai à próxima Indaba. E a brincar a brincar já estamos quase em Abrl, mês de LTS!"
thumbnail = "images/e188.png"

featured = false
categories = ["Episódio"]
tags = [
  "lxd",
  "cloud-init",
  "flutter",
  "hugo",
  "indaba",
  "appimages",
  "lxd",
  "dma",
  "lxc",
  "ubuntu",
  "Collabora",
  "LibreTrend",
  "Slimbook",
  "Radio Zero",
  "Sr Podcast",
]
seasons = ["S01"]
aliases = ["e188", "E188"]
+++

São tempos de mudança! …pelo menos para o outro podcast do Carrondo. Já o Firefox do Miguel continua doente e o Constantino foi ao Algarve, mas a fingir. Há novidades, para variar, no mundo UBPorts e o Mark vai à próxima Indaba. E a brincar a brincar já estamos quase em Abrl, mês de LTS!
Já sabem: oiçam, subscrevam e partilhem!

* https://www.youtube.com/watch?v=2FJTGT9RTp0
* https://www.youtube.com/watch?v=GKXZPhhSJ1M
* https://www.youtube.com/watch?v=8SpilmmSDY0
* https://www.youtube.com/watch?v=eOKIS5DCAvQ
* https://www.youtube.com/watch?v=8EEmd8c0-4U
* https://twitter.com/ubuntu/status/1507457063965843459
* https://github.com/ubports/ubports-installer/releases/tag/0.9.5-beta
* https://web.archive.org/web/20220319135808/https://ubports.com/pt/blog/2022-a-new-year-a-new-initiative-ubuntu-teach-10/post/2022-a-new-year-a-new-initiative-ubports-teach-3839#scrollTop=0
* https://ubports.com/pt/blog/2022-a-new-year-a-new-initiative-ubuntu-teach-10/post/2022-a-new-year-a-new-initiative-ubports-teach-3839#scrollTop=0
* https://twitter.com/fredldotme/status/1504874822660153344
* https://twitter.com/fredldotme/status/1504911659093090307
* https://twitter.com/edri/status/1507365213879910407
* https://edri.org/our-work/the-digital-markets-act-promises-to-free-people-from-digital-walled-gardens/
* https://shop.nitrokey.com/shop/product/nk-pro-2-nitrokey-pro-2-3?aff_ref=3
* https://shop.nitrokey.com/shop?aff_ref=3
* https://youtube.com/PodcastUbuntuPortugal


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

