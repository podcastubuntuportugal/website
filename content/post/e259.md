+++
title = "E259 Avô, anda jogar no meu Minitel"
itunes_title = "Avô, anda jogar no meu Minitel"
episode = 259
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e259/e259.mp3"
podcast_duration = "0:57:08"
podcast_bytes = "82286967"
author = "Podcast Ubuntu Portugal"
date = "2023-08-10"
description = "O Tiago não pôde estar presente e telefonou-nos aflito a dizer que fez um buraquinho num disco de látex vulcanizado - e com isto queremos dizer que furou um pneu. Esta semana a nostalgia acompanhou-nos: andámos em feiras de antiguidades a explorar electrónica antiga e a descobrir redes de dados primitivas (com uma surpresa inesperada), elogiou-se a Mozilla, Professores, Médicos e Enfermeiros e o Elon Musk (calma...) e ainda trouxemos boas notícias de Ubuntu Touch e Ubports e conversas de velhinhos sobre jogos de computador."
thumbnail = "images/e259.png"

featured = false
categories = ["Episódio"]
tags = [
  "Ubuntu",
  "Linux",
  "Software Livre",
  "Open Source Software",
  "Festa do Software Livre 2023",
  "FSL",
  "FSL2023",
  "nvme-cli",
  "nvme",
  "Samsung",
  "Zigbee",
  "Sonoff",
  "Aqara",
  "Nabu Casa",
  "Home Assistant",
  "Home Automation",
  "Sensores",
  "Home Assistant Yellow",
  "Yellow",
  "Raspberry Pi",
  "Home Assistant SkyConnect",
  "Matter",
  "CPHA",
  "Centro Linux",
  "MILL",
  "Makers In Little Lisbon",
  "Freedom Not Fear",
  "GLUA",
  "Grupo de Linux da Universdade de Aveiro",
  "Ubuntu Summit",
  "Humble Bundle",
  "Nitrokey",
  "ANACOM",
  "Sr Podcast",
]
seasons = ["S01"]
aliases = ["e259", "E259"]
+++

O Tiago não pôde estar presente e telefonou-nos aflito a dizer que fez um buraquinho num disco de látex vulcanizado - e com isto queremos dizer que furou um pneu. Esta semana a nostalgia acompanhou-nos: andámos em feiras de antiguidades a explorar electrónica antiga e a descobrir redes de dados primitivas (com uma surpresa inesperada), elogiou-se a Mozilla, Professores, Médicos e Enfermeiros e o Elon Musk (calma...) e ainda trouxemos boas notícias de Ubuntu Touch e Ubports e conversas de velhinhos sobre jogos de computador.

Já sabem: oiçam, subscrevam e partilhem!

* https://larevuedesmedias.ina.fr/du-minitel-linternet
* https://www.minitel.org
* https://fr.wikipedia.org/wiki/Minitel
* https://masto.pt/@arturcoelho/110819890252848858
* https://arstechnica.com/gaming/2019/01/tales-of-an-aging-gamer-why-dont-i-pick-up-a-controller-as-often-as-i-used-to
* https://hub.ubuntu-pt.org/apps/forms/s/TQkCj6wX5JSydjadfHFk4zYW
* https://loco.ubuntu.com/events/ubuntu-pt/4370-encontro-ubuntu-pt-sintra
* https://t.me/UBports_pi/7159
* https://techcrunch.com/2023/07/24/as-twitter-destroys-its-brand-by-renaming-itself-x-mastodon-usage-numbers-are-again-soaring
* https://ubports.com/pt/blog/ubports-blogs-noticias-1/post/ubuntu-touch-ota-2-focal-release-3894
* https://web.archive.org/web/2/https://www.mozilla.org/pt-PT/products/vpn
* https://web.archive.org/web/20230730193626/https://ubports.com/pt/blog/ubports-blogs-noticias-1/post/ubuntu-touch-ota-2-focal-release-3894
* https://www.mozilla.org/pt-PT/products/vpn
* https://www.youtube.com/watch?v=HsiI4Ytm0vA
* https://ubuntu.com/blog/ubuntu-summit-2023
* https://freedomnotfear.org/2023/fnf23-participant-information
* https://debconf23.debconf.org/
* https://ubuconla.org/
* https://www.ubucon.asia/
* https://2023.pycon.pt
* https://festa2023.softwarelivre.eu
* https://ubuconpt2032.ubuntu-pt.org
* https://loco.ubuntu.com/teams/ubuntu-pt/
* https://shop.nitrokey.com/shop?aff_ref=3
* https://masto.pt/@pup
* https://youtube.com/PodcastUbuntuPortugal


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

