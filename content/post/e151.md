+++
title = "E151 Ah! A audácia!"
itunes_title = "Ah! A audácia!"
episode = 151
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e151/e151.mp3"
podcast_duration = "0:57:26"
podcast_bytes = "55602319"
author = "Podcast Ubuntu Portugal"
date = "2021-07-15"
description = "O Constantino andou a fazer prospecção de piqueniques, o Carrondo já se despachou das mudanças, e entretanto o Nextcloud festejou o seu quinto aniversário lançando a versão 22. O Audacity deu, e dá, que falar…"
thumbnail = "images/e151.png"

featured = false
categories = ["Episódio"]
tags = [
  "firefox",
  "nextcloud 22",
  "audacity",
  "ubuntu",
  "LibreTrend",
  "Slimbook",
  "Radio Zero",
  "Sr Podcast",
]
seasons = ["S01"]
aliases = ["e151", "E151"]
+++

O Constantino andou a fazer prospecção de piqueniques, o Carrondo já se despachou das mudanças, e entretanto o Nextcloud festejou o seu quinto aniversário lançando a versão 22. O Audacity deu, e dá, que falar…
Já sabem: oiçam, subscrevam e partilhem!

* https://addons.mozilla.org/pt-PT/firefox/addon/cookie-quick-manager/
* https://addons.mozilla.org/pt-PT/firefox/addon/profile-switcher/
* https://addons.mozilla.org/pt-PT/firefox/addon/darkreader/
* https://www.youtube.com/watch?v=Y0VZ7t8JGZE
* https://github.com/audacity/audacity/discussions/889
* https://github.com/audacity/audacity/discussions/889
* https://web.archive.org/web/20210706125644/https://github.com/audacity/audacity/discussions/889
* https://web.archive.org/web/20210706130028/https://github.com/audacity/audacity/discussions/1225
* https://web.archive.org/web/20210706150802/https://www.audacityteam.org/about/desktop-privacy-notice/
* https://keychronwireless.referralcandy.com/3P2MKM7
* https://shop.nitrokey.com/shop/product/nk-pro-2-nitrokey-pro-2-3?aff_ref=3
* https://shop.nitrokey.com/shop?aff_ref=3
* https://youtube.com/PodcastUbuntuPortugal



### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

